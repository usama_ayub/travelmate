angular.module('app', ['ionic','ngCordova'])
  .run(function ($ionicPlatform, $ionicHistory, $state, $ionicPopup, $rootScope, $ionicLoading, $timeout, $window) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      /*var expireDate = new Date();
      $timeout(function () {
        localStorage.removeItem('email');
        localStorage.removeItem('password');
      }, expireDate.getDate() + 1);*/
      if (localStorage.getItem('email') || localStorage.getItem('password')) {
        $state.go('app.selectStatus');
      }


      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
      if (window.Connection) {
        if (navigator.connection.type == Connection.NONE) {
          $ionicPopup.alert({
            title: 'No Internet Connection',
            template: 'The Internet is discounted on your devices.'
          });
        }
      }
      $rootScope.logout = function () {
        $ionicLoading.show({template: 'Logging out....'});

        $timeout(function () {
          $ionicLoading.hide();
          $ionicHistory.clearCache();
          $ionicHistory.clearHistory();
          $ionicHistory.removeBackView();
          /*$cookies.remove('email');
           $cookies.remove('password');*/
          localStorage.removeItem('email');
          localStorage.removeItem('password');
          $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
          $state.go('login');
        }, 300);
      }
    });
    $ionicPlatform.registerBackButtonAction(function (event) {
      console.log($state.current.name);
      if ($state.current.name) {
        navigator.app.backHistory();
      }
      else {
        navigator.app.backHistory();
      }
    }, 100);
  })
  .config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider

    // setup an abstract state for the tabs directive
      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl',
        controllerAs: 'login'
      })
      .state('forgotPassword', {
        url: '/forgotPassword',
        templateUrl: 'templates/forgotPassword.html',
        controller: 'forgotPasswordCtrl',
        controllerAs: 'forgot'
      })
      .state('register', {
        url: '/register',
        templateUrl: 'templates/register.html',
        controller: 'registerCtrl',
        controllerAs: 'register'
      })
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/sideMenu.html',
        controller: 'profileCtrl',
        controllerAs: 'profile'
      })

      .state('app.selectStatus', {
        url: '/selectStatus',
        views: {
          'menuContent': {
            templateUrl: 'templates/selectStatus.html'
          }
        }
      })
      .state('app.report', {
        url: '/report',
        views: {
          'menuContent': {
            templateUrl: 'templates/report.html',
            controller: 'reportCtrl',
            controllerAs: 'report'
          }
        }
      })
      .state('app.selectCountry', {
        url: '/selectCountry',
        views: {
          'menuContent': {
            templateUrl: 'templates/selectCountry.html',
            controller: 'selectCountryCtrl'
          }
        }
      })
      .state('app.category', {
        url: '/category/:categoryID',
        views: {
          'menuContent': {
            templateUrl: 'templates/category.html',
            controller: 'categoryCtrl',
            controllerAs: 'category'
          }
        }
      })
      .state('app.visaForm', {
        url: '/visaForm',
        views: {
          'menuContent': {
            templateUrl: 'templates/visaForm.html',
            controller: 'visaFormCtrl',
            controllerAs: 'visaForm'
          }
        }
      })
      .state('app.attachPassport', {
        url: '/attachPassport',
        views: {
          'menuContent': {
            templateUrl: 'templates/attachPassport.html',
            controller: 'visaFormCtrl',
            controllerAs: 'visaForm'
          }
        }
      })
      .state('app.attachPassportPic1', {
        url: '/attachPassportPic1',
        views: {
          'menuContent': {
            templateUrl: 'templates/attachPassportPic1.html',
            controller: 'visaFormCtrl',
            controllerAs: 'visaForm'
          }
        }
      })
      .state('app.attachPic', {
        url: '/attachPic',
        views: {
          'menuContent': {
            templateUrl: 'templates/attachPic.html'
          }
        }
      })
      .state('app.attachPic1', {
        url: '/attachPic1',
        views: {
          'menuContent': {
            templateUrl: 'templates/attachPic1.html'
          }
        }
      })
      .state('app.term&condition', {
        url: '/term&condition',
        views: {
          'menuContent': {
            templateUrl: 'templates/term&condition.html'
          }
        }
      })
      .state('app.billingInfo', {
        url: '/billingInfo',
        views: {
          'menuContent': {
            templateUrl: 'templates/billingInfo.html',
            controller: 'visaFormCtrl',
            controllerAs: 'visaForm'
          }
        }
      })
      .state('app.creditInfo', {
        url: '/creditInfo',
        views: {
          'menuContent': {
            templateUrl: 'templates/creditInfo.html',
            controller: 'visaFormCtrl',
            controllerAs: 'visaForm'
          }
        }
      })
      .state('app.visaPreview', {
        url: '/visaPreview',
        views: {
          'menuContent': {
            templateUrl: 'templates/visaPreview.html',
            controller: 'visaFormCtrl',
            controllerAs: 'visaForm'
          }
        }
      })
      .state('app.visaPreview2', {
        url: '/visaPreview2',
        views: {
          'menuContent': {
            templateUrl: 'templates/visaPreview2.html',
            controller: 'previewDetail',
            controllerAs: 'preview'
          }
        }
      })
      .state('app.invoice', {
        url: '/invoice',
        views: {
          'menuContent': {
            templateUrl: 'templates/invoice.html',
            controller: 'visaFormCtrl',
            controllerAs: 'visaForm'
          }
        }
      })
      .state('app.invoice2', {
        url: '/invoice2',
        views: {
          'menuContent': {
            templateUrl: 'templates/invoice2.html',
            controller: 'invoiceCtrl',
            controllerAs: 'invoice'
          }
        }
      })
      .state('app.selectProcess', {
        url: '/selectProcess',
        views: {
          'menuContent': {
            templateUrl: 'templates/selectProcess.html',
            controller: 'visaFormCtrl',
            controllerAs: 'visaForm'
          }
        }
      })
      .state('app.processed', {
        url: '/processed',
        views: {
          'menuContent': {
            templateUrl: 'templates/processed.html'
          }
        }
      })
      .state('app.profile', {
        url: '/profile',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile.html'
          }
        }
      })

      .state('app.contact', {
        url: '/contact',
        views: {
          'menuContent': {
            templateUrl: 'templates/contact.html'
          }
        }
      })
      .state('app.about', {
        url: '/about',
        views: {
          'menuContent': {
            templateUrl: 'templates/about.html'
          }
        }
      })
      .state('app.visaApply', {
        url: '/visaApply',
        views: {
          'menuContent': {
            templateUrl: 'templates/visaApply.html'
          }
        }
      });


    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');

  });

(function () {
  "use strict";
  angular.module('app')
    .controller('categoryCtrl', function ($http, API, $scope, $state, $stateParams) {
      var self = this;
      var id = $stateParams.categoryID;
      console.log(id);
      API.categoryData(id)
        .then(function (data) {
          $scope.ee = {};
          $scope.change = function (d) {
           var data =  JSON.parse(d);
            console.log(data);
            localStorage.setItem('VisaTypeID', data.VisaTypeID);
            localStorage.setItem('Price', data.Price);
            localStorage.setItem('VisaType', data.VisaType);
          };
          $scope.detail = data;
          console.log($scope.detail);
        })
        .catch(function (error) {
          console.log(error);
        });
    })


})();


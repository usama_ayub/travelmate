(function () {
  "use strict";
  angular.module('app')
    .controller('forgotPasswordCtrl', function (API, $state, $ionicPopup,$rootScope) {
      var self = this;

      /*Forgot Password*/

      this.forgot = {
        email: this.email
      };
      this.submit = function () {
        API.password(self.forgot)
          .then(function (data) {
            if (data == "SUCCESSFUL") {
              console.log(data);
              $ionicPopup.alert({
                title: 'Note!!',
                template: 'Email is Successfully Send'

              }).then(function(res) {
                $state.go('login');
              });
            }
            else
              $ionicPopup.alert({
                title: 'Error',
                template: 'email is not available'
              });
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    })
})();

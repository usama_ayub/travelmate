(function () {
  "use strict";
  angular.module('app')
    .controller('invoiceCtrl', function ($http, API, $scope, $state, $ionicLoading,$timeout) {
      var self = this;
      $ionicLoading.show({
        content: '<ion-spinner icon="ios"></ion-spinner>'
      });
      $timeout(function () {
        $ionicLoading.hide()
      }, 900);
      this.PaymentInfoID = localStorage.getItem('detail');
      $scope.invoiceData = null;

      this.Invoice = {
        PaymentInfoID: this.PaymentInfoID
      };
      API.Invoice_detail(self.Invoice)
        .then(function (data) {
          $scope.invoiceData = data;
          console.log($scope.invoiceData);
          console.log(data);
          console.log(self.Invoice);
        })
        .catch(function (error) {
          console.log(error);
        });
    })

})();


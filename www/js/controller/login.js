(function () {
  "use strict";
  angular.module('app')
    .controller('loginCtrl', function (API, $state, $ionicPopup, $rootScope, $timeout, $ionicLoading) {
      var self = this;

      /*Login*/

      this.user = {email: this.email, password: this.password};
      this.submit = function () {
        $ionicLoading.show({
          content: '<ion-spinner icon="ios"></ion-spinner>'
        });
        $timeout(function () {
          $ionicLoading.hide()
        }, 900);
        API.login(self.user)
          .then(function (data) {
            console.log(self.user);

            $rootScope.loginData = self.user;
            console.log(data);
            if (data == "SUCCESSFUL") {
              /*var expireDate = new Date();
               expireDate.setDate(expireDate.getDate() + 45);
               $cookies.get('email');
               $cookies.get('password');
               // Setting a cookie
               $cookies.put('email', self.user.email,{'expires': expireDate});
               $cookies.put('password', self.user.password,{'expires': expireDate});*/

              var expireDate = new Date();
              $timeout(function () {
                localStorage.setItem('email', self.user.email);
                localStorage.setItem('password', self.user.password);
              }, expireDate.getDate() + 44);

              $state.go('app.selectStatus');
            }
            else
              $ionicPopup.alert({
                title: 'Error',
                template: 'Email and password is invalid'
              });
          })
          .catch(function (error) {
            console.log(error);
          })
      };

    })
})();

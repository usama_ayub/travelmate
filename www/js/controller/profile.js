(function () {
  "use strict";
  angular.module('app')
    .controller('profileCtrl', function (API, $ionicPopup, $rootScope,$ionicLoading) {
      /*console.log($rootScope.loginData);
      var loginData = $rootScope.loginData;*/
      var loginData = $rootScope.loginData;
      var self = this;
      API.profile(loginData)
        .then(function (data) {
          self.profileData = data;
          console.log(data);
          localStorage.setItem('memberID', data[0].MemberID);
          /*console.log(self.profileData);*/
        })
        .catch(function (error) {
          console.log(error);
        });
      this.memberid = localStorage.getItem('memberID');
      console.log(self.memberid);
      this.profile = {
        memberid: this.memberid,
        email: this.email,
        name: this.name,
        password: this.password,
        phone: this.phone
      };
      this.editProfile = function () {
        API.ProfileEdit(self.profile)
          .then(function (data) {
            console.log(self.profile);
            /*console.log(self.profileData);*/
            if (data == "SUCCESSFUL") {
              console.log(data);
              $ionicPopup.alert({
                title: 'Profile Edit',
                template: 'successfully updated your profile'
              }).then(function(res) {
                console.log('Thank you for not eating my delicious ice cream cone');
              });
              window.location.reload();
            }
          })
          .catch(function (error) {
            console.log(error);
          });
      };

    })
})();

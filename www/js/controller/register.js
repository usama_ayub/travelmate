(function () {
  "use strict";
  angular.module('app')
    .controller('registerCtrl', function (API, $state, $ionicPopup, $rootScope, $ionicLoading, $timeout) {
      var self = this;
      var date = new Date();
      var d = date.toDateString();
      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      var year = date.getFullYear();
      this.user = {
        email: this.email,
        name: this.name,
        password: this.password,
        phone: this.phone,
        signupdate: dd + '/' + mm + '/' + year
      };
      this.submit = function () {
          $ionicLoading.show({
            content: '<ion-spinner icon="ios"></ion-spinner>'
          });
        $timeout(function () {
          $ionicLoading.hide()
        }, 900);
        API.register(self.user)
          .then(function (data) {
            console.log(self.user);
            $rootScope.loginData = self.user;
            if (data == "SUCCESSFUL") {
              console.log(data);
              $state.go('app.selectStatus');
              console.log("User Create");
            }
            else
              $ionicPopup.alert({
                title: 'Error',
                template: 'Email already in use'
              });
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    })
})();

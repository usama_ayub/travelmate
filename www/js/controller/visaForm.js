(function () {
  "use strict";
  angular.module('app')
    .controller('visaFormCtrl', function ($http, API, $scope, $state, $ionicPopup, $rootScope) {
      var self = this;
      var date = new Date();
      $scope.imgUrl;
      $scope.dataImg;
      this.d = date.toDateString();
      this.dd = date.getDate();
      this.mm = date.getMonth() + 1;
      this.year = date.getFullYear();
      this.MemberID = localStorage.getItem('memberID');
      this.CountryID = sessionStorage.getItem('CountryID');
      this.VisaTypeID = localStorage.getItem('VisaTypeID');
      this.VisaType = localStorage.getItem('VisaType');
      this.Amount = localStorage.getItem('Price');
      this.VisaFormID = localStorage.getItem('visaFormID');

      this.visaFormData = {
        CountryID: this.CountryID,
        VisaTypeID: this.VisaTypeID,
        MemberID: this.MemberID,
        Name: this.Name,
        FatherHusbandName: this.FatherHusbandName,
        PassportNumber: this.PassportNumber,
        SkywardsNumber: this.SkywardsNumber,
        Dependents: this.Dependents,
        DateOfBirth: this.DateOfBirth,
        StayHotel: this.StayHotel,
        StaySelf: this.StaySelf,
        ContactNoPakistan: this.ContactNoPakistan,
        OtherContactNo: this.OtherContactNo,
        Nationality: this.Nationality,
        IssuancePassportDay: this.IssuancePassportDay,
        IssuancePassportMonth: this.IssuancePassportMonth,
        IssuancePassportYear: this.IssuancePassportYear,
        ExpiryPassportDay: this.ExpiryPassportDay,
        ExpiryPassportMonth: this.ExpiryPassportMonth,
        ExpiryPassportYear: this.ExpiryPassportYear,
        PassportPic: this.PassportPic,
        Photo: this.Photo,
        SubmissionDate: this.dd + '/' + this.mm + '/' + this.year
      };
      this.submitVisa = function () {
        localStorage.setItem('visaForm', JSON.stringify(this.visaFormData));
      };
      $scope.visaObj = JSON.parse(localStorage.getItem('visaForm'));
      this.visaSubmit = function () {
        API.visaForm($scope.visaObj)
          .then(function (ID) {
            if (ID) {
              localStorage.setItem('visaFormID', JSON.stringify(ID));
            }

          })
          .catch(function (error) {
            console.log(error);
          });
       };


      this.billingData = {
        FirstName: this.FirstName,
        LastName: this.LastName,
        Address: this.Address,
        ContactNo: this.ContactNo,
        EmailAddress: this.EmailAddress,
        CCFirstName: this.CCFirstName,
        CCLastName: this.CCLastName,
        CCType: this.CCType,
        ExpiryDateMonth: this.ExpiryDateMonth,
        ExpiryDateDay: this.ExpiryDateDay,
        ExpiryDateYear: this.ExpiryDateYear,
        MemberID: this.MemberID,
        VisaFormID: this.VisaFormID,
        Amount: this.Amount
      };
      this.submitBilling = function () {
        localStorage.setItem('billing', JSON.stringify(this.billingData));
      };
      $scope.billObj = JSON.parse(localStorage.getItem('billing'));
      this.billingSubmit = function () {
        API.billing($scope.billObj)
          .then(function (data) {
            if (data == "SUCCESSFUL") {
              console.log(data);
              console.log($scope.billObj);
            }
          })
          .catch(function (error) {
            console.log(error);
          });
      }

    })


})();


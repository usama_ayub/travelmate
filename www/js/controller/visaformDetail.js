(function () {
  "use strict";
  angular.module('app')
    .controller('previewDetail', function ($http, API, $scope, $state, $ionicLoading,$timeout) {
      var self = this;
      $ionicLoading.show({
        content: '<ion-spinner icon="ios"></ion-spinner>'
      });
      $timeout(function () {
        $ionicLoading.hide()
      }, 900);
      this.VisaFormID = localStorage.getItem('detail');
      $scope.previewData = null;

      this.preview = {
        VisaFormID: this.VisaFormID
      };
      API.Visaform_detail(self.preview)
        .then(function (data) {
          $scope.previewData = data;
          console.log($scope.previewData);
          console.log(data);
          console.log(self.preview);
        })
        .catch(function (error) {
          console.log(error);
        });
    })


})();


(function () {
  "use strict";
  angular.module('app')
    .service('API', function ($q, $http) {
      var self = this;

      /*Login*/
      this.login = function (user) {
        return $q(function (resolve, reject) {
          $http.post('http://www.appmetrikglobal.com/travelmate/member.php?email=' + user.email + '&password=' + user.password + '&action=authenticateUser')
            .then(function (res) {
              console.log(user);
              console.log(res.data);
              resolve(res.data)
            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })
      };

      /*Forget Password*/
      this.password = function (forgot) {
        return $q(function (resolve, reject) {
          $http.get('http://appmetrikglobal.com/travelmate/forgotpassword.php?email=' + forgot.email + '&action=ForgotPassword')
            .then(function (res) {
              console.log(res.data);
              resolve(res.data)

            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })
      };


      /*Profile Data*/
      this.profile = function (loginData) {
        return $q(function (resolve, reject) {
          $http.post('http://appmetrikglobal.com/travelmate/memberdetails.php?email=' + (localStorage.getItem('email') || loginData.email))
          /*$http.post('http://appmetrikglobal.com/travelmate/memberdetails.php?email=a@b.com')*/
            .then(function (res) {
              console.log(res.data);
              resolve(res.data)
            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })
      };

      /*Profile Edit*/
      this.ProfileEdit = function (profile) {
        return $q(function (resolve, reject) {
          $http.get('http://www.appmetrikglobal.com/travelmate/member.php?memberid=' + profile.memberid + '&email=' + profile.email + '&name=' + profile.name + '&password=' + profile.password + '&phone=' + profile.phone + '&action=UpdateMember')
            .then(function (res) {
              console.log(res.data);
              resolve(res.data)
            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })
      };
      /*Register*/
      this.register = function (user) {
        return $q(function (resolve, reject) {
          $http.get('http://www.appmetrikglobal.com/travelmate/member.php?email=' + user.email + '&name=' + user.name + '&password=' + user.password + '&phone=' + user.phone + '&signupdate=' + user.signupdate + '&action=SignUpUser')
            .then(function (res) {
              console.log(res.data);
              resolve(res.data)
            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })
      };

      /*Select Country Data*/
      var countryData;
      this.selectCountry = function () {
        return $q(function (resolve, reject) {
          $http.get("http://www.appmetrikglobal.com/travelmate/country.php")
            .then(function (res) {
              console.log(res.data);
              countryData = res.data;
              resolve(res.data);
            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })

      };

      /*Category Data*/
      this.categoryData = function (data) {
        console.log(data);
        if (data) {
          return $q(function (resolve, reject) {
            $http.get("http://www.appmetrikglobal.com/travelmate/visastype.php")
              .then(function (res) {
                for (var i = 0; i <= res.data.length - 1; i++) {
                  console.log(res.data);
                  resolve(res.data)
                }
              })
              .catch(function (error) {
                console.log(error);
                reject(error)
              })
          })
        }
      };
      /*VisaForm*/
      this.visaForm = function (visaForm) {
        return $q(function (resolve, reject) {
          $http.get('http://www.appmetrikglobal.com/travelmate/visa.php?CountryID=' + visaForm.CountryID + '&VisaTypeID=' + visaForm.VisaTypeID + '&MemberID=' + visaForm.MemberID + '&Name=' + visaForm.Name + '&FatherHusbandName=' + visaForm.FatherHusbandName + '&PassportNumber=' + visaForm.PassportNumber + '&SkywardsNumber=' + visaForm.SkywardsNumber + '&Dependents=' + visaForm.Dependents + '&DateOfBirth=' + visaForm.DateOfBirth + '&StayHotel=' + visaForm.StayHotel + '&StaySelf=' + visaForm.StaySelf + '&ContactNoPakistan=' + visaForm.ContactNoPakistan + '&OtherContactNo=' + visaForm.OtherContactNo + '&Nationality=' + visaForm.Nationality + '&IssuancePassportDay=' + visaForm.IssuancePassportDay + '&IssuancePassportMonth=' + visaForm.IssuancePassportMonth + '&IssuancePassportYear=' + visaForm.IssuancePassportYear + '&ExpiryPassportDay=' + visaForm.ExpiryPassportDay + '&ExpiryPassportMonth=' + visaForm.ExpiryPassportMonth + '&ExpiryPassportYear=' + visaForm.ExpiryPassportYear + '&PassportPic=' + visaForm.PassportPic + '&Photo=' + visaForm.Photo + '&SubmissionDate=' + visaForm.SubmissionDate + '&action=VisaFormAdd')
            .then(function (res) {
              console.log(res.data);
              resolve(res.data)
            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })
      };
/*Report*/
      this.report = function (report) {
        return $q(function (resolve, reject) {
          $http.get('http://www.appmetrikglobal.com/travelmate/checkstatus.php?memberid=' + report.memberid)
            .then(function (res) {
              console.log(res.data);
              resolve(res.data)
            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })
      };

      this.Visaform_detail = function (preview) {
        return $q(function (resolve, reject) {
          $http.get('http://appmetrikglobal.com/travelmate/visaformdetail.php?VisaFormID=' + preview.VisaFormID)
            .then(function (res) {
              console.log(res.data);
              resolve(res.data)
            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })
      };

      this.Invoice_detail = function (Invoice) {
        return $q(function (resolve, reject) {
          $http.get('http://appmetrikglobal.com/travelmate/paymentinfodetail.php?PaymentInfoID=' + Invoice.PaymentInfoID)
            .then(function (res) {
              console.log(res.data);
              resolve(res.data)
            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })
      };
      /*Billing And Credit card Info*/
      this.billing = function (billing) {
        return $q(function (resolve, reject) {

          $http.get('http://www.appmetrikglobal.com/travelmate/payment.php?FirstName=' + billing.FirstName + '&LastName=' + billing.LastName + '&Address=' + billing.Address + '&ContactNo=' + billing.ContactNo + '&EmailAddress=' + billing.EmailAddress + '&CCFirstName=' + billing.CCFirstName + '&CCLastName=' + billing.CCLastName + '&CCType=' + billing.CCType + '&ExpiryDateMonth=' + billing.ExpiryDateMonth + '&ExpiryDateDay=' + billing.ExpiryDateDay + '&ExpiryDateYear=' + billing.ExpiryDateYear + '&MemberID=' + billing.MemberID + '&VisaFormID=' + billing.VisaFormID + '&Amount=' + billing.Amount  + '&action=PaymentFormAdd')
            .then(function (res) {
              console.log(res.data);
              resolve(res.data)
            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })
      };

      /*Picture Upload*/
      this.uploaded = function () {
        return $q(function (resolve, reject) {
          $http.get('http://appmetrikglobal.com/travelmate/passport/')
            .then(function (res) {
              console.log(res.data);
              resolve(res.data)
            })
            .catch(function (error) {
              console.log(error);
              reject(error)
            })
        })
      };

        this.VisasTypeID = function (VisasTypeID) {
          return $q(function (resolve, reject) {
            $http.get('http://appmetrikglobal.com/travelmate/visastypedetails.php?VisasTypeID='+ VisasTypeID.VisaTypeID)
              .then(function (res) {
                console.log(res.data);
                resolve(res.data)
              })
              .catch(function (error) {
                console.log(error);
                reject(error)
              })
          })
        };
    })
})();
